import collections
import importlib
import os
import sys
import uuid
from importlib import metadata
from xml.etree.ElementTree import fromstring

from drb.topics.topic import DrbTopic
from drb.topics.dao import ManagerDao, YamlDao
from drb.drivers.xml import XmlNode
from drb.exceptions.core import DrbException

import yaml

from jsonschema.exceptions import ValidationError


def check_cortex():
    result = {}
    eps = metadata.entry_points()
    for ep in eps['drb.topic']:

        try:
            module = importlib.import_module(ep.value)
        except ModuleNotFoundError as ex:
            raise DrbException(f'Invalid entry point {ep.name}: {ex.msg}')

        try:
            path = os.path.join(os.path.dirname(module.__file__), 'cortex.yml')
            YamlDao.validate(path)
        except (FileNotFoundError, ValidationError) as ex:
            raise DrbException(
                f'Invalid item class description(s) from {ep.name}: {ex.msg}')

        with open(path) as file:
            data = yaml.safe_load_all(file)
            for ic_data in data:
                try:
                    ic = DrbTopic(**ic_data)
                except (KeyError, DrbException):
                    raise DrbException(f'Failed to load item class: {ic_data}')
                if ic.id in result:
                    raise DrbException(
                        f'Item class definition conflict: id ({ic.id}) used '
                        f'by {result[ic.id].label} and {ic.label}')
                else:
                    result[ic.id] = ic


class TopicTest:

    def __init__(self, path: str):
        self.loader = ManagerDao()

        file = open(os.path.join(path),
                    'r')
        self.dicts = list(yaml.safe_load_all(file))
        file.close()

    def check(self, class_test):
        # Check the signatures to load
        check_cortex()
        # Check if all mandatory fields are here
        for yml in self.dicts:
            class_test.assertIsNotNone(yml.get('id'))
            class_test.assertIsNotNone(yml.get('match').get('category'))
            class_test.assertIsNotNone(yml.get('match').get('label'))
        # Check if no duplicate in the ids
        list_uuid = collections.Counter([x.get('id') for x in self.dicts])
        for key in list_uuid:
            class_test.assertEqual(list_uuid[key], 1)

    def test_id(self, class_test):
        for data in self.dicts:
            ic = self.loader.get_drb_topic(uuid.UUID(data.get('id')))
            class_test.assertEqual(uuid.UUID(data.get('id')), ic.id)

    def test_label(self, class_test):
        for data in self.dicts:
            ic = self.loader.get_drb_topic(uuid.UUID(data.get('id')))
            class_test.assertEqual(data.get('match').get('label'), ic.label)

    def test_category(self, class_test):
        for data in self.dicts:
            ic = self.loader.get_drb_topic(uuid.UUID(data.get('id')))
            class_test.assertEqual(data.get('match').get('category'),
                                   ic.category.value)

    def mandatory_field(self, class_test):
        self.test_id(class_test)
        self.test_label(class_test)
        self.test_category(class_test)

    def test_parent(self, class_test):
        for data in self.dicts:
            ic = self.loader.get_drb_topic(uuid.UUID(data.get('id')))
            match = data.get('match')
            if match.get('parent'):
                class_test.assertEqual(uuid.UUID(match.get('parent')),
                                       ic.subClassOf[0]
                                       )
                class_test.assertTrue(self.loader.is_subclass(
                    ic, self.loader.get_drb_topic(
                        uuid.UUID(match.get('parent')
                                  ))))

    def test_description(self, class_test):
        for data in self.dicts:
            ic = self.loader.get_drb_topic(uuid.UUID(data.get('id')))
            match = data.get('match')
            if match.get('description'):
                class_test.assertEqual(
                    match.get('description').strip().replace('\n', ' '),
                    ic.description.strip().replace('\n', ' ')
                )
            else:
                class_test.assertIsNone(ic.description)

    def test_factory(self, class_test):
        for data in self.dicts:
            ic = self.loader.get_drb_topic(uuid.UUID(data.get('id')))
            match = data.get('match')
            if match.get('factory'):
                class_test.assertIsNotNone(ic.factory)
            else:
                class_test.assertIsNone(ic.factory)

    def optional_field(self, class_test):
        self.test_parent(class_test)
        self.test_factory(class_test)
        self.test_description(class_test)

    def match_good_nodes(self, class_test):
        for data in self.dicts:
            ic = self.loader.get_drb_topic(uuid.UUID(data.get('id')))
            good_nodes = [XmlNode(fromstring(prd))
                          for prd in data.get('match').get('match_nodes')
                          ]
            for good_prd in good_nodes:
                class_test.assertTrue(ic.matches(good_prd))

    def match_bad_nodes(self, class_test):
        for data in self.dicts:
            ic = self.loader.get_drb_topic(uuid.UUID(data.get('id')))
            if 'not_match_nodes' in data.get('match').keys():
                bad_nodes = [XmlNode(fromstring(prd))
                             for prd in data.get('match').get(
                        'not_match_nodes'
                    )
                             ]
                for bad_prd in bad_nodes:
                    class_test.assertFalse(ic.matches(bad_prd))

    def test(self, class_test):
        self.check(class_test)
        self.mandatory_field(class_test)
        self.optional_field(class_test)
        self.match_good_nodes(class_test)
        self.match_bad_nodes(class_test)
